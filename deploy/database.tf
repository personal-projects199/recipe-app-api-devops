resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id,
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

# Security groups allow you to control the inbound and outbound access allowed to that resource.
# In this case we are creating a group to have an inbound access in port 5432 (default port of postgres)
resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  # egress{} = outbound access
  # inbound access
  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    security_groups = [
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id #Give access to our ECS service from our database.
    ]
  }

  tags = local.common_tags
}


resource "aws_db_instance" "main" {
  # Database identifier used to access the database in our console.
  identifier = "${local.prefix}-db"
  name       = "recipe"
  # 20GB storage
  allocated_storage = 20
  # gp2 = SSD General purpose 2 (Entry-level storage type in AWS)
  storage_type         = "gp2"
  engine               = "postgres"
  engine_version       = "11.15"
  instance_class       = "db.t2.micro"
  db_subnet_group_name = aws_db_subnet_group.main.name
  username             = var.db_username
  password             = var.db_password
  # Number of days you want to backup your data.
  backup_retention_period = 0
  # Determines if the database should be run on multiple availability zones
  multi_az               = false
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )

}